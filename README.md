## Note

Please add the package's repository into your module dependencies in the package.json file.

```json
{
   "dependencies" : {
       "smartdrive-lib" : "git+https://suritch@bitbucket.org/sukritch/smartdrive-lib.git#master"
   }
}
```