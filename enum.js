'use strict';

/* Module dependencies */
var Enum = require('enum');

/****************
* Exported enum *
****************/

/**
* SYSA = System Administrator
* SUBA = Sub-System Administrator
* MEMU = Member User
*/
module.exports.oauthUserRole = new Enum({MEMU:0, SUBA:1, SYSA:2});

/**
* GTW = Gateway
* APP = Application
* SRV = Server
*/
module.exports.oauthDeviceType = new Enum({GTW:0, APP:1, SRV:2});
