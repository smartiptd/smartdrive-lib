'use strict';

/* Common constants */
define('BLANK', '');
define('SPACE', ' ');
define('SLASH', '/');

/* Role-Based */
define('SYSTEM_ADMIN', 'SYSA');
define('SUBSYS_ADMIN', 'SUBA');
define('MEMBER_USER', 'MEMU');

/**
* Define constant properties
*
* @param {string} name
* @param {Object} value
*/
function define(name, value) {
    Object.defineProperty(exports, name, {
        value: value,
        enumerable: true
    });
}
