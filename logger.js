'use strict';

/* Module dependencies */
var winston = require('winston');
var path = require('path');
var common = require('./common');
var constant = require('./constant');

/* Log file & directory */
var projectPath = common.getProjectPath();
var logPath = projectPath + '/.bin/log';

common.createDirectory(logPath);

var filename_info = path.join(logPath, 'info.log');
var filename_warn = path.join(logPath, 'warn.log');
var filename_error = path.join(logPath, 'error.log');
var filename_exce = path.join(logPath, 'exception.log');

/* Configuration */
var config = {
    levels: {
		debug: 0,
		info: 1,
		warn: 2,
		error: 3
    },
    colors: {
        debug: 'blue',
        info: 'green',
        warn: 'yellow',
        error: 'red'
    }
}

/* Export module */
var logger = module.exports = new (winston.Logger) ({
    transports:[
        new (winston.transports.Console)({
            level: 'info',
            colorize: true,
            timestamp: common.getCurrentDateTime,
            humanReadableUnhandledException: true
        }),
        new (winston.transports.File)({
            level: 'info',
            name: 'info-file',
            filename: filename_info,
            timestamp: common.getCurrentDateTime,
            colorize: true,
			json: true,
            prettyPrint: true,
			logstash: true,
			maxsize: 102400,
			maxFiles: 5,
			tailable: true,
			maxRetries: 5,
            zippedArchive: true
        }),
        new (winston.transports.File)({
            level: 'warn',
            name: 'warn-file',
            filename: filename_warn,
            timestamp: common.getCurrentDateTime,
            colorize: true,
			json: true,
            prettyPrint: true,
			logstash: true,
			maxsize: 102400,
			maxFiles: 5,
			tailable: true,
			maxRetries: 5,
            zippedArchive: true
        }),
        new (winston.transports.File)({
            level: 'error',
            name: 'error-file',
            filename: filename_error,
            timestamp: common.getCurrentDateTime,
            colorize: true,
			json: true,
            prettyPrint: true,
			logstash: true,
			maxsize: 102400,
			maxFiles: 5,
			tailable: true,
			maxRetries: 5,
            zippedArchive: true
        })
    ],
    exceptionHandlers:[
        new (winston.transports.File)({
            level: 'error',
            name: 'exception-file',
            filename: filename_exce,
            timestamp: common.getCurrentDateTime,
            colorize: true,
			maxsize: 102400,
			maxFiles: 5,
			tailable: true,
			maxRetries: 5,
			zippedArchive: true,
			humanReadableUnhandledException: true
        })
    ],
    levels: config.levels,
    colors: config.colors,
	exitOnError: false
});
