'use strict';

/* Module dependencies */
var moment = require('moment');
var path = require('path');
var mkdir = require('mkdirp');
var crypto = require('crypto');
var constant = require('./constant');

/* Export function */
module.exports = {

    /**
    * Check validation of an object
    *
    * @param {Object} obj
    */
    checkValidate: function(obj) {
        return (obj === null || typeof obj === 'undefined') ? false : true;
    },

    /**
    * Verify that type of object is function
    *
    * @param {Object} obj
    */
    checkFunction: function(obj) {
        return (typeof obj === 'function') ? true : false;
    },

    /**
    * Get project path from anywhere
    *
    */
    getProjectPath: function() {
        return path.resolve(__dirname).replace('/node_modules/smartdrive-lib', constant.BLANK);
    },

    /**
    * Get current data time string
    *
    */
    getCurrentDateTime: function() {
        return moment(Date.now()).format('YYYY-MM-DD HH:mm A');
    },

    /**
    * Create a new directory and any subdirectories at dir
    *
    * @param {String} path
    */
    createDirectory: function(path) {
        mkdir.sync(path);
    },

    /**
    * Encryption data with signature
    *
    * @param {String} text
    * @param {String} signature
    */
    encrypt: function(text, signature) {
        var cipher = crypto.createCipher('aes-256-ctr', signature);
        var encrypted = cipher.update(text.toString(), 'utf8', 'base64');
        encrypted += cipher.final('base64');
        return encrypted;
    },

    /**
    * Decryption data with signature
    *
    * @param {String} text
    * @param {String} signature
    */
    decrypt: function(encrypted, signature) {
        var decipher = crypto.createDecipher('aes-256-ctr', signature);
        var decrypted = decipher.update(encrypted.toString(), 'base64', 'utf8');
        decrypted += decipher.final('utf8');
        return decrypted;
    },

    /**
    * Generate random integer between two numbers 'low' and 'high'
    *
    * @param {Number} low
    * @param {Number} high
    */
    randomInt: function(low, high) {
        return Math.floor(Math.random() * (high - low + 1) + low);
    },

    /**
    * Generate random string with specified length
    *
    * @param {Number} len
    */
    randomStr: function(len) {
        return crypto.randomBytes(Math.ceil(len * 3 / 4))
                .toString('base64')
                .slice(0, len)
                .replace(/\+/g, '0')
                .replace(/\//g, '0');
    }
}
